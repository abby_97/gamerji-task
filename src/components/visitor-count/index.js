import React, { useState } from "react";
import "./visitor-count.css";
function VisitorCount() {
  const [inputValue, setInputValue] = useState("");
  const [result, setResult] = useState("");
  const [isValidInput, setIsValidInput] = useState(true);

  const countVisitors = () => {
    // Validate input using the regex pattern
    const validInputPattern = /^[WMC]+$/;
    if (!validInputPattern.test(inputValue)) {
      setIsValidInput(false);
      setResult("NA");
      return;
    }

    setIsValidInput(true);

    // Rest of the counting logic
    const counts = {
      W: 0,
      M: 0,
      C: 0,
    };

    for (let i = 0; i < inputValue.length; i++) {
      const char = inputValue[i];
      switch (char) {
        case "W":
          counts["W"]++;
          break;
        case "M":
          counts["M"]++;
          break;
        case "C":
          counts["C"]++;
          break;
        default:
          break;
      }
    }

    const sortedCounts = Object.entries(counts) // creates an array of arrays with the key and value as array elements
      .sort((a, b) => b[1] - a[1]) // sorts the array in descending order from the second element of each array
      .map(([char, count]) => `${count}${char}`) // creates a new array from the sorted array with the count and char joined
      .join(""); // joins the array into a string

    setResult(sortedCounts);
  };

  return (
    <div>
      <label>Enter visits:</label>
      <input
        type="text"
        value={inputValue}
        onChange={(e) => setInputValue(e.target.value)}
      />
      <button onClick={countVisitors}>Count</button>
      {!isValidInput && (
        <p className="invalid-input">
          Please enter a valid input (consisting of 'W', 'M', or 'C'
          characters).
        </p>
      )}
      <p>Result: {result}</p>
    </div>
  );
}

export default VisitorCount;
