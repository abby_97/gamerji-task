import React from 'react';
import VisitorCount from './components/visitor-count';

function App() {
  return (
    <div>
      <h1>Visitor Count</h1>
      <VisitorCount />
    </div>
  );
}

export default App;